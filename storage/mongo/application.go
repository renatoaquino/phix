package mongo

import (
	"time"

	"bitbucket.org/renatoaquino/phix.git/entities"
	uuid "github.com/satori/go.uuid"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Application persistence interface.
type Application struct {
	coll *mgo.Collection
}

func (app Application) Get(ID string) (*entities.Application, error) {
	var res entities.Application
	err := app.coll.Find(bson.M{"_id": ID}).One(&res)
	return &res, err
}

func (app Application) Save(data entities.Application) error {
	if data.ID == "" {
		data.ID = uuid.NewV4().String()
		data.CreatedAt = time.Now()
	}
	data.UpdatedAt = time.Now()
	return app.coll.Insert(data)
}

func (app Application) Delete(ID string) error {
	_, err := app.coll.RemoveAll(bson.M{"_id": ID})
	return err
}
func (app Application) ListByCompanyID(ID string) ([]entities.Application, error) {
	results := []entities.Application{}
	err := app.coll.Find(bson.M{"company_id": ID}).All(&results)
	return results, err
}

func (app Application) List() ([]entities.Application, error) {
	results := []entities.Application{}
	err := app.coll.Find(bson.M{}).All(&results)
	return results, err
}
