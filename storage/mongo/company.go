package mongo

import (
	"time"

	"bitbucket.org/renatoaquino/phix.git/entities"
	uuid "github.com/satori/go.uuid"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//Company persistence
type Company struct {
	Coll *mgo.Collection
	str  *Store
}

func (c Company) Get(ID string) (*entities.Company, error) {
	var cmp entities.Company
	err := c.Coll.Find(bson.M{"_id": ID}).One(&cmp)
	return &cmp, err
}

func (c Company) Save(cmp *entities.Company) error {
	if cmp.ID == "" {
		cmp.ID = uuid.NewV4().String()
		cmp.CreatedAt = time.Now()
	}
	cmp.UpdatedAt = time.Now()
	return c.Coll.Insert(cmp)
}

func (c Company) Delete(ID string) error {
	_, err := c.Coll.RemoveAll(bson.M{"_id": ID})
	return err
}

func (c Company) List() (*[]entities.Company, error) {
	results := []entities.Company{}
	err := c.Coll.Find(bson.M{}).All(&results)
	return &results, err
}

func (c Company) ListByUserID(ID string) (*[]entities.Company, error) {
	results := []entities.Company{}
	return &results, nil
}
