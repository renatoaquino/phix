package mongo

import (
	"errors"
	"log"
	"time"

	uuid "github.com/satori/go.uuid"

	"bitbucket.org/renatoaquino/phix.git/entities"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Token persistence interface.
type Token struct {
	Coll *mgo.Collection
}

func (tkn Token) Get(ID string) (*entities.Token, error) {
	var res *entities.Token
	err := tkn.Coll.Find(bson.M{"_id": ID}).One(res)
	if err != nil {
		log.Fatal(err)
		return nil, errors.New("Token not found")
	}
	return res, nil
}
func (tkn Token) Save(data *entities.Token) error {
	if data.ID == "" {
		data.ID = uuid.NewV4().String()
		data.CreatedAt = time.Now()
	}
	data.UpdatedAt = time.Now()
	return tkn.Coll.Insert(data)

}
func (tkn Token) Delete(ID string) error {
	_, err := tkn.Coll.RemoveAll(bson.M{"_id": ID})
	return err
}
func (tkn Token) ListByApplicationID(ID string) (*[]entities.Token, error) {
	results := []entities.Token{}
	err := tkn.Coll.Find(bson.M{"application_id": ID}).All(&results)
	return &results, err
}
func (tkn Token) List() (*[]entities.Token, error) {
	results := []entities.Token{}
	err := tkn.Coll.Find(bson.M{}).All(&results)
	return &results, err
}
