package mongo

import (
	"errors"
	"time"

	uuid "github.com/satori/go.uuid"

	"bitbucket.org/renatoaquino/phix.git/entities"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//User in memory user persistence
type User struct {
	Coll *mgo.Collection
}

// Save persists an User into memory
func (ui User) Save(usr *entities.User) error {
	if usr.ID == "" {
		usr.ID = uuid.NewV4().String()
		usr.CreatedAt = time.Now()
	}
	if usr.Salt == "" {
		return errors.New("salt cannot be empty")
	}
	if usr.Password == "" {
		return errors.New("password cannot be empty")
	}
	usr.UpdatedAt = time.Now()
	return ui.Coll.Insert(usr)
}

// Get retrieves an User by ID
func (ui User) Get(ID string) (*entities.User, error) {
	var usr entities.User
	err := ui.Coll.Find(bson.M{"_id": ID}).One(&usr)
	if err != nil {
		return nil, errors.New("User not found")
	}
	return &usr, nil
}

// Delete removes a user.
// Returns an error if it do not exists.
func (ui User) Delete(ID string) error {
	return ui.Coll.RemoveId(ID)
}

// Authenticate standardizes the way password hashes are generated to identify an User
func (ui User) Authenticate(login string, pass string) (*entities.User, error) {
	usr := &entities.User{}
	err := ui.Coll.Find(bson.M{"$or": []bson.M{bson.M{"id": login}, bson.M{"email": login}}}).One(usr)
	pwd := usr.EncodePassword(pass, usr.Salt)
	if pwd != usr.Password {
		return nil, errors.New("invalid password")
	}
	return usr, err
}

func (ui User) ListByCompanyID(ID string) (*[]entities.User, error) {
	results := []entities.User{}
	return &results, nil
}

func (ui User) Count() int {
	count, _ := ui.Coll.Count()
	return count
}
