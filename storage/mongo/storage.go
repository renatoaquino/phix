package mongo

import (
	"log"

	"bitbucket.org/renatoaquino/phix.git/config"
	"bitbucket.org/renatoaquino/phix.git/storage/interfaces"
	mgo "gopkg.in/mgo.v2"
)

const (
	ApplicationCollection = "applications"
	CompanyCollection     = "companies"
	TokenCollection       = "tokens"
	UserCollection        = "users"
)

type Store struct {
	db *mgo.Database
}

func FromConfig(c config.Config) Store {
	storage, err := mgo.Dial(c.GetString("MONGO_URL"))
	if err != nil {
		log.Fatal(err)
	}

	//mgo.SetLogger(c.Log)
	//mgo.SetDebug(true)

	return Store{storage.DB(c.GetString("MONGO_DB"))}
}

func (s Store) Application() interfaces.Application {
	return Application{s.db.C(ApplicationCollection)}
}

func (s Store) Company() interfaces.Company {
	return Company{s.db.C(CompanyCollection), &s}
}

func (s Store) Token() interfaces.Token {
	return Token{s.db.C(TokenCollection)}
}

func (s Store) User() interfaces.User {
	return User{s.db.C(UserCollection)}
}

func TestConfig() config.Config {
	cfg, _ := config.Default("")
	cfg.Define("DB_TYPE", "mongo", "Which database flawor should we use?", "Databases")
	cfg.Define("MONGO_URL", "localhost", "Mongo Connection URL", "Databases")
	cfg.Define("MONGO_DB", "test", "Mongo database name", "Databases")

	storage := FromConfig(cfg)
	storage.db.C(ApplicationCollection).DropCollection()
	storage.db.C(CompanyCollection).DropCollection()
	storage.db.C(TokenCollection).DropCollection()
	storage.db.C(UserCollection).DropCollection()

	return cfg
}
