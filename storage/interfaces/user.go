package interfaces

import "bitbucket.org/renatoaquino/phix.git/entities"

// User defines the interface for user persistence and access.
type User interface {
	Get(string) (*entities.User, error)
	Save(*entities.User) error
	Delete(string) error
	Authenticate(login string, pass string) (*entities.User, error)
	ListByCompanyID(string) (*[]entities.User, error)
	Count() int
}
