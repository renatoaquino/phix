package interfaces

import "bitbucket.org/renatoaquino/phix.git/entities"

// Token persistence interface.
type Token interface {
	Get(string) (*entities.Token, error)
	Save(*entities.Token) error
	Delete(string) error
	ListByApplicationID(string) (*[]entities.Token, error)
	List() (*[]entities.Token, error)
}
