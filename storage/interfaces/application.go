package interfaces

import "bitbucket.org/renatoaquino/phix.git/entities"

// Application persistence interface.
type Application interface {
	Get(string) (*entities.Application, error)
	Save(entities.Application) error
	Delete(string) error
	ListByCompanyID(string) ([]entities.Application, error)
	List() ([]entities.Application, error)
}
