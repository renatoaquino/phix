package interfaces

import "bitbucket.org/renatoaquino/phix.git/entities"

// Company defines the interface for company persistence and access.
type Company interface {
	Get(string) (*entities.Company, error)
	Save(*entities.Company) error
	Delete(string) error
	ListByUserID(string) (*[]entities.Company, error)
	List() (*[]entities.Company, error)
}
