package storage

import (
	"testing"

	"bitbucket.org/renatoaquino/phix.git/config"
	"bitbucket.org/renatoaquino/phix.git/storage/interfaces"
	"bitbucket.org/renatoaquino/phix.git/storage/mongo"
)

var (
	ui  interfaces.User
	cfg config.Config
)

func TestMongo(t *testing.T) {
	//Setup
	cfg = mongo.TestConfig()
	storage := FromConfig(cfg)
	ui = storage.User()

	t.Run("", FullStack) // user_test.go

	//Teardown
}

func FullStack(t *testing.T) {
	t.Run("User Tests", UserTests) // user_test.go
}
