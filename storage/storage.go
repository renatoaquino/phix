package storage

import (
	"context"
	"fmt"
	"log"
	"sort"
	"strings"

	"bitbucket.org/renatoaquino/phix.git/config"
	"bitbucket.org/renatoaquino/phix.git/storage/interfaces"
	"bitbucket.org/renatoaquino/phix.git/storage/mongo"
)

type key int

const storeKey key = 0

// DbTypes is a list of the implemented database storages.
var DbTypes = []string{"mongo"}

// Store is the main interface for data persistence
// All access to the persisted data should go through this interface.
type Store interface {
	Application() interfaces.Application
	Company() interfaces.Company
	Token() interfaces.Token
	User() interfaces.User
}

// FromConfig returns an storage for the given DB_TYPE
func FromConfig(c config.Config) Store {
	dbType := strings.ToLower(c.GetString("DB_TYPE"))
	sort.Strings(DbTypes)
	i := sort.SearchStrings(DbTypes, dbType)
	if i > len(DbTypes) {
		log.Fatal(fmt.Sprintf("Unimplemented database type %s", dbType))
	}

	switch dbType {
	case "mongo":
		return mongo.FromConfig(c)
	default:
		log.Fatal(fmt.Sprintf("Unknow database type %s", c.GetString("DB_TYPE")))
	}
	return nil
}

// NewContext generates a new Context storing the Store into its values.
// Thats helpfull if you need to transfer the store inside the context
// to another function.
func NewContext(ctx context.Context, s Store) context.Context {
	return context.WithValue(ctx, storeKey, s)
}

// FromContext retrieves a *Store previously added to the context by the NewContext func.
// It returns nil if no Store is found.
func FromContext(ctx context.Context) (Store, bool) {
	s, ok := ctx.Value(storeKey).(Store)
	return s, ok
}
