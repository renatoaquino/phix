package storage

import (
	"testing"

	"bitbucket.org/renatoaquino/phix.git/entities"
)

var (
	salt = "USR_PASSWD_SALT"
	uid1 string
	uid2 string
)

func UserTests(t *testing.T) {
	t.Run("save user", UserSave)
	t.Run("get user", GetUser)
	t.Run("authentication", AuthenticateUser)
	t.Run("delete user", DeleteUser)
}

func UserSave(t *testing.T) {

	// Empty Salt
	usr := &entities.User{Name: "Test User", Email: "test@host.com"}
	err := ui.Save(usr)
	if err == nil {
		t.Fatalf("should not save an user with an empty salt")
	}
	usr.Salt = salt

	err = ui.Save(usr)
	if err == nil {
		t.Fatalf("should not save an user with an empty password")
	}
	usr.Password = usr.EncodePassword("pw1", salt)

	err = ui.Save(usr)
	if err != nil {
		t.Fatalf("error while saving user %s", err)
	}
	if ui.Count() != 1 {
		t.Errorf("should have only one user, %d found", ui.Count())
	}
	if usr.ID == "" {
		t.Errorf("user.ID should be filled")
	}
	uid1 = usr.ID

	usr2 := &entities.User{Name: "Test User 2", Email: "test2@host.com", Salt: salt}
	usr2.Password = usr2.EncodePassword("pw2", salt)
	err = ui.Save(usr2)
	if err != nil {
		t.Fatalf("Error while saving user %s", err)
	}
	if ui.Count() != 2 {
		t.Errorf("should have two users, %d found", ui.Count())
	}
	if usr2.ID == "" {
		t.Errorf("user.ID should be filled")
	}
	uid2 = usr2.ID

}

func GetUser(t *testing.T) {
	usr, err := ui.Get(uid1)
	if err != nil {
		t.Fatalf("Error while retrieving user \"%s\"", err)
		return
	}

	if usr.Name != "Test User" {
		t.Fatalf("Unknow user returned \"%s\"", usr.Name)
	}
}

func AuthenticateUser(t *testing.T) {
	usr, err := ui.Authenticate("test@host.com", "pw1")
	if err != nil {
		t.Fatalf("Error authenticating user \"%s\"", err)
		return
	}

	if usr.Name != "Test User" {
		t.Fatalf("Unexpected user returned \"%s\"", usr.Name)
	}

	usr2, err := ui.Authenticate("test2@host.com", "pw2")
	if err != nil {
		t.Fatalf("Error authenticating user \"%s\"", err)
		return
	}

	if usr2.Name != "Test User 2" {
		t.Fatalf("Unexpected user returned \"%s\"", usr2.Name)
	}
}

func DeleteUser(t *testing.T) {
	err := ui.Delete(uid1)
	if err != nil {
		t.Fatalf("Error while removing user \"%s\"", err)
		return
	}

	usr, err := ui.Get(uid1)
	if err == nil || usr != nil {
		t.Fatalf("User 1 should not exist")
		return
	}

	usr, err = ui.Get(uid2)
	if usr == nil {
		t.Fatalf("User 2 should not be deleted")
		return
	}
}
