package entities

import (
	"crypto/sha1"
	"encoding/base64"
	"time"
)

type User struct {
	ID        string    `bson:"_id" json:"id"`
	Name      string    `json:"name"`
	Password  string    `json:"-"`
	Salt      string    `json:"-"`
	Email     string    `json:"email"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

// EncodePassword is the standard user password enconding
func (u *User) EncodePassword(password string, salt string) string {
	bv := []byte(password + salt)
	hasher := sha1.New()
	hasher.Write(bv)
	return base64.URLEncoding.EncodeToString(hasher.Sum(nil))
}
