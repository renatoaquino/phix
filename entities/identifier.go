package entities

//Identifier is an abstraction of a field filter to find an entitie
type Identifier struct {
	Name  string
	Value string
}
