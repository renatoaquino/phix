package entities

import "time"

type Token struct {
	ID            string
	Name          string
	ApplicationID string
	CreatedAt     time.Time
	UpdatedAt     time.Time
}
