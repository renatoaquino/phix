package entities

import "time"

type Application struct {
	ID        string
	Name      string
	CompanyID string
	CreatedAt time.Time
	UpdatedAt time.Time
}
