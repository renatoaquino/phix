package entities

import "time"

/*
A Company its the primary client of this application.

Every Company could have many `User` and `Application`.
*/
type Company struct {
	ID        string
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
}
