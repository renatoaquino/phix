/*
 * Actions describe changes of state in your application
 */

// We import constants to name our actions' type
import {
  CHANGE_FORM,
  SENDING_REQUEST,
  LOGIN_REQUEST,
  LOGOUT,
  REQUEST_ERROR,
  CLEAR_ERROR,
  AUTH_SET_TOKEN,
  SET_LOGIN_FORWARD_PATH
} from './constants'

import {login} from '../api'
/**
 * Sets the form state
 * @param  {object} newFormState          The new state of the form
 * @param  {string} newFormState.username The new text of the username input field of the form
 * @param  {string} newFormState.password The new text of the password input field of the form
 */
export function changeForm (newFormState) {
  return {type: CHANGE_FORM, newFormState}
}

/**
 * Sets the authentication token
 * @param  {string} token
 */
export function setToken (data) {
  return {type: AUTH_SET_TOKEN, ...data}
}

/**
 * Sets the `currentlySending` state, which displays a loading indicator during requests
 * @param  {boolean} sending True means we're sending a request, false means we're not
 */
export function sendingRequest (sending) {
  return {type: SENDING_REQUEST, sending}
}

/**
 * Tells the app we want to log in a user
 * @param  {object} data          The data we're sending for log in
 * @param  {string} data.username The username of the user to log in
 * @param  {string} data.password The password of the user to log in
 */
export function loginRequest (data) {
  return {type: LOGIN_REQUEST, data}
}

/**
 * Saves the forward path to execute after a successful login
 * @param {string} path The forward path
 */
export function setLoginForwardPath(path){
  return {type: SET_LOGIN_FORWARD_PATH,path: path}
}

/**
 * Tells the app we want to log out a user
 */
export function logout () {
}

/**
 * Sets the `error` state to the error received
 * @param  {object} error The error we got when trying to make the request
 */
export function requestError (error) {
  return {type: REQUEST_ERROR, error}
}

/**
 * Sets the `error` state as empty
 */
export function clearError () {
  return {type: CLEAR_ERROR}
}

export function authorize ({username, password}) {
  return function (dispatch){
    dispatch({type: CLEAR_ERROR})
    dispatch({type: SENDING_REQUEST, sending: true})
    try{
      return login(username,password)
             .then(json => {
              if(json.access_token){
                var t = new Date()

                t.setSeconds(json.expires_in)
                
                dispatch(setToken({token:json.access_token,expires:t}))
              }
             }).catch(function(error){
              dispatch(requestError(error))
             });
    }finally{
      dispatch({type: SENDING_REQUEST, sending: false})
    }
  }
}

export function logoutRequest () {
  return function (dispatch){
      dispatch({type: LOGOUT})
      dispatch(setToken({token:'',expires:new Date(1970,0,1)}))
  }
}