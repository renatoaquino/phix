import {
  AUTH_SET_TOKEN,
  SET_LOGIN_FORWARD_PATH
} from '../actions/constants'

const TOKEN_NAME = 'phix:token'
const EXPIRES_NAME = 'phix:token_expires'

function fromMilliseconds(secs) {
    var t = new Date(1970, 0, 1); // Epoch
    t.setMilliseconds(secs);
    return t;
}

var token   = localStorage.getItem(TOKEN_NAME) || ''
var expires = fromMilliseconds(localStorage.getItem(EXPIRES_NAME)) || new Date(1970,0,1)

if(expires < Date()){
  localStorage.removeItem(TOKEN_NAME)
  localStorage.removeItem(EXPIRES_NAME)
  token = ''
  expires = 0
}

// The initial session state
let initialState = {
  token: token,
  expires: expires,
  forward: {pathname:'/'}
}

function setToken(token,expires){
      localStorage.setItem(TOKEN_NAME,token)
      localStorage.setItem(EXPIRES_NAME,expires.getTime())
      initialState.token = token
      initialState.expires = expires
      return {token: token, expires: expires}
}

// Takes care of changing the session state
export default function session (state = initialState, action) {
  switch (action.type) {
    case AUTH_SET_TOKEN:
      return {...state, ...setToken(action.token,action.expires)}
    case SET_LOGIN_FORWARD_PATH:
      return {...state, forward: action.path}
    default:
      return state
  }
}

export function getToken(){
  return initialState.token
}