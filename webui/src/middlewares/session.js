import {AUTH_SET_TOKEN,} from '../actions/constants'
import {LOCATION_CHANGE} from 'react-router-redux'
import {setLoginForwardPath,logoutRequest} from '../actions'
//import {SET_LOGIN_FORWARD_PATH} from '../actions/constants'
import {push} from 'react-router-redux'

function sessionMiddleware({getState,dispatch}){
    return function (next) {
        return function (action) {
            var state;
            if (action.type === LOCATION_CHANGE){
                state = getState()
                if(action.payload.pathname === '/logout'){
                    dispatch(logoutRequest())
                }
                if(typeof state.session !== 'undefined' && typeof state.session.token !== 'undefined' && state.session.token !== ''){
                    next(action);
                }else if(typeof action.payload !== 'undefined' && typeof action.payload.pathname !== 'undefined'){
                    console.log(action.payload.pathname)
                    if(action.payload.pathname !== '/login' && action.payload.pathname !== '/logout'){
                        dispatch(setLoginForwardPath(action.payload))
                        dispatch(push({pathname:'/login'}))
                        return
                    }
                }
            }

            var result = next(action)

            if (action.type === AUTH_SET_TOKEN) {
                state = getState()
                dispatch(setLoginForwardPath({pathname:'/'}))
                if(state.session.forward === '/login' || state.session.forward === '/logout' ){
                    return dispatch(push({pathname:'/'}))
                }else{
                    return dispatch(push(state.session.forward))
                }
            }

            return result
        }
    }
}

export default sessionMiddleware