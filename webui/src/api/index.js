import {SERVER_HOST} from '../config'
import {getToken} from '../reducers/session'
import 'whatwg-fetch'

function handleErrors(response) {
    if (!response.ok) {
        return response.json().then(data=>{
          return Promise.reject(data.message)
        })
    }
    return response;
}

export function login(username,password){
    var data = new FormData()
    data.append('login', username)
    data.append('password', password)
    return fetch(SERVER_HOST+'/login', {
            method: 'POST',
            body: data
        }).then(handleErrors)
        .then(response => {
            return response.json()
        })    
}

export function me(){
    var rh = new Headers();
    rh.set('Authorization','Bearer '+getToken())
    return fetch(SERVER_HOST+'/me', {
            method: 'GET',
            headers: rh
        }).then(handleErrors)
        .then(response => {
            return response.json()
        })    
}