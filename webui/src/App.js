import React, { Component } from 'react'
import {createStore,applyMiddleware,combineReducers} from 'redux'
import * as appreducers from './reducers'
import {Provider} from 'react-redux'
import {createLogger} from 'redux-logger'
import thunk from 'redux-thunk'
import {Route} from 'react-router-dom'
import { ConnectedRouter, routerReducer, routerMiddleware } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'
import sessionMiddleware from './middlewares/session'
import './App.css';

import Login from './components/Login.js';
import Home from './components/Home.js';

const history = createHistory()
let middlewares = [thunk,sessionMiddleware,routerMiddleware(history)]

if (process.env.NODE_ENV !== 'production'){
  middlewares.push(createLogger({
    // Ignore `CHANGE_FORM` actions in the logger, since they fire after every keystroke
    predicate: (getState, action) => action.type !== 'CHANGE_FORM'
  }))
}

let reducers = combineReducers({...appreducers,router:routerReducer})
let store = createStore(reducers,applyMiddleware(...middlewares))

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <div>
            <Route exact path="/" component={Home}/>
            <Route path="/login" component={Login}/>
          </div>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
