package main

import (
	"context"
	"flag"
	llog "log"
	"os"

	"bitbucket.org/renatoaquino/phix.git/config"
	"bitbucket.org/renatoaquino/phix.git/entities"
	"bitbucket.org/renatoaquino/phix.git/log"
	"bitbucket.org/renatoaquino/phix.git/server"
	"bitbucket.org/renatoaquino/phix.git/storage"
)

// Config returns the default configuration for the app
// mixed with the config file and environment variables.
func Config() config.Config {
	configfile := flag.String("c", "config/phix.conf", "The configuration file")
	dumpconfig := flag.Bool("d", false, "Dumps the parsed configuration")
	flag.Parse()

	cfg, err := config.Default(*configfile)
	if err != nil {
		llog.Fatal(err)
		os.Exit(1)
	}

	if *dumpconfig {
		cfg.Dump()
		os.Exit(0)
	}

	return cfg
}

func Setup(ctx context.Context) {
	logger, ok := log.FromContext(ctx)
	if !ok {
		llog.Fatal("cannot access the default logger")
	}
	store, ok := storage.FromContext(ctx)
	if !ok {
		logger.Fatal("cannot access the default storage")
	}
	usr := store.User()
	if usr.Count() == 0 {
		logger.Print("adding default user admin@phix.")
		_usr := entities.User{}
		_usr.Name = "Phix Admin"
		_usr.Email = "admin@phix"
		_usr.Salt = "F0C0DA81"
		_usr.Password = _usr.EncodePassword("phix", _usr.Salt)
		usr.Save(&_usr)
	}
}

func main() {
	cfg := Config()
	logger := log.FromConfig(cfg)
	store := storage.FromConfig(cfg)

	ctx := context.Background()
	ctx = config.NewContext(ctx, cfg)
	ctx = log.NewContext(ctx, logger)
	ctx = storage.NewContext(ctx, store)

	Setup(ctx)

	server.Start(ctx)
}
