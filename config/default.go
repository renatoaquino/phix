package config

import (
	"context"
	"os"

	"github.com/renatoaquino/goderp"
)

type key int

const configKey key = 0

type Config interface {
	EnableEnv()
	Define(string, interface{}, string, string)
	Get(string) interface{}
	GetInt(string) int
	GetFloat(string) float64
	GetString(string) string
	GetBool(string) bool
	GetDescription(string) string
	GetGroup(string) string
	GetDefault(string) interface{}
	Parse(string) error
	Dump()
}

func Default(configfile string) (Config, error) {
	var c Config = goderp.New()
	c.Define("PORT", 8888, "Service Port", "Daemon")
	c.Define("PATH_PREFIX", "", "Service Path Prefix", "Daemon")
	c.Define("BEHIND_PROXY", false, "Is the service behind a proxy?", "Daemon")
	c.Define("ENABLE_PROFILER", false, "Enable the profiler handlers?", "Daemon")
	c.Define("DB_TYPE", "mongo", "Which database flawor should we use?", "Databases")
	c.Define("MONGO_URL", "localhost", "Mongo Connection URL", "Databases")
	c.Define("MONGO_DB", "phix", "Mongo database name", "Databases")
	c.Define("LOG_TYPE", "local", "The log system to use", "Logging")
	c.Define("LOG_LEVEL", "info", "Log Level", "Logging")
	c.Define("LOG_HOST", "127.0.0.1:514", "Log Level", "Logging")
	c.Define("LOG_FACILITY", "local0", "Log Facility", "Logging")
	c.Define("LOG_PROTOCOL", "tcp", "Use tcp or udp for logging", "Logging")
	c.Define("LOG_TAG", "phix", "Logging tag", "Logging")
	c.Define("PASSWORD_SALT", "63a8c03", "The Password SALT", "Security")
	c.Define("JWT_SECRET", "1d61c498533d8cba1658d8949286006732fb30e9", "The JWT Crypt Secret", "Security")
	c.Define("JWT_EXPIRES", 86400, "JWT Token Expire time in seconds", "Security")
	c.EnableEnv()

	_, err := os.Stat(configfile)
	if err != nil {
		//log.Printf("Config file is missing: %s", configfile)
		return c, nil
	}

	err = c.Parse(configfile)
	if err != nil {
		return nil, err
	}
	return c, nil
}

// NewContext generates a new Context storing the Config into its values.
// Thats helpfull if you need to transfer the config inside the context
// to another function.
func NewContext(ctx context.Context, c Config) context.Context {
	return context.WithValue(ctx, configKey, c)
}

// FromContext retrieves a *Store previously added to the context by the NewContext func.
// It returns nil if no Store is found.
func FromContext(ctx context.Context) (Config, bool) {
	s, ok := ctx.Value(configKey).(Config)
	return s, ok
}
