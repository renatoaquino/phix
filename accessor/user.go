package accessor

import (
	"context"

	"bitbucket.org/renatoaquino/phix.git/entities"
	"bitbucket.org/renatoaquino/phix.git/errors"
	"bitbucket.org/renatoaquino/phix.git/middleware"
	"bitbucket.org/renatoaquino/phix.git/storage"
)

func Me(ctx context.Context) (*entities.User, *errors.Error) {
	token := middleware.GetToken(ctx)
	if token == nil {
		return nil, errors.TokenRequired
	}
	storage, ok := storage.FromContext(ctx)
	if !ok {
		return nil, errors.Database
	}
	usrStore := storage.User()
	if usrStore == nil {
		return nil, errors.Database
	}
	claims := middleware.TokenClaims(token)
	if claims == nil {
		return nil, errors.InvalidToken
	}
	usr, err := usrStore.Get(claims["id"].(string))
	if err != nil {
		return nil, errors.Database
	}
	return usr, nil
}
