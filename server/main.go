package server

import (
	"context"
	"fmt"
	llog "log"
	"net/http"
	"net/http/pprof"

	"bitbucket.org/renatoaquino/phix.git/config"
	"bitbucket.org/renatoaquino/phix.git/handler"
	"bitbucket.org/renatoaquino/phix.git/log"
	"bitbucket.org/renatoaquino/phix.git/middleware"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func attachProfiler(router *mux.Router) {
	router.HandleFunc("/debug/pprof/", pprof.Index)
	router.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	router.HandleFunc("/debug/pprof/profile", pprof.Profile)
	router.HandleFunc("/debug/pprof/symbol", pprof.Symbol)

	router.Handle("/debug/pprof/goroutine", pprof.Handler("goroutine"))
	router.Handle("/debug/pprof/heap", pprof.Handler("heap"))
	router.Handle("/debug/pprof/threadcreate", pprof.Handler("threadcreate"))
	router.Handle("/debug/pprof/block", pprof.Handler("block"))
}

func wrapHandler(ctx context.Context, h http.Handler) http.Handler {
	log, ok := log.FromContext(ctx)
	if !ok {
		llog.Fatal("Unable to retrieve default logger!")
	}
	config, ok := config.FromContext(ctx)
	if !ok {
		log.Fatal("Unable to retrieve default config!")
	}
	if config.GetBool("BEHIND_PROXY") {
		h = handlers.ProxyHeaders(h)
	}

	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Authorization"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})

	h = handlers.CORS(originsOk, headersOk, methodsOk)(h)

	//h = middleware.CorsHandler(h)
	h = middleware.ContextHandler(ctx, h)
	return h
}

// Start initializes routes and serves http
func Start(ctx context.Context) {
	log, ok := log.FromContext(ctx)
	if !ok {
		llog.Fatal("Unable to retrieve default logger!")
	}
	config, ok := config.FromContext(ctx)
	if !ok {
		log.Fatal("Unable to retrieve default config!")
	}

	r := mux.NewRouter().PathPrefix(fmt.Sprintf("/%s/", config.GetString("PATH_PREFIX"))).Subrouter()
	attachProfiler(r)

	// Behold the handlers
	r.Path("/healthcheck").Handler(handler.Healthcheck{})
	r.Path("/robots.txt").Handler(handler.Robots{})
	r.Path("/login").HandlerFunc(handler.Auth{}.LoginHandler)
	r.Path("/graphiql").HandlerFunc(handler.ServeGraphiQL)
	r.Path("/graphql").Handler(middleware.RequireAuthFunc(handler.ServeGraphQL))
	r.Path("/me").Handler(middleware.RequireAuthFunc(handler.User{}.Me))

	// TODO: Config flag to print or a verbose mode?
	r.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		t, err := route.GetPathTemplate()
		if err != nil {
			return err
		}
		log.Println(t)
		return nil
	})

	http.ListenAndServe(fmt.Sprintf(":%d", config.GetInt("PORT")), wrapHandler(ctx, r))
}
