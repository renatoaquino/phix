package handler

import (
	"context"
	"fmt"
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"

	"bitbucket.org/renatoaquino/phix.git/config"
	"bitbucket.org/renatoaquino/phix.git/entities"
	"bitbucket.org/renatoaquino/phix.git/storage"
)

// AuthResponse holds all the information needed to access
// endpoints after a login.
// Every request to the API must have a header Authorization: <TokenType> <Token>
type AuthResponse struct {
	Token     string `json:"access_token"`
	TokenType string `json:"token_type"` // Bearer
	ExpiresIn int    `json:"expires_in"`
}

// Auth handles logins
type Auth struct{}

// LoginHandler performs the autentication process and returns an access token
func (a Auth) LoginHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	login := r.PostFormValue("login")
	password := r.PostFormValue("password")

	store, ok := storage.FromContext(ctx)
	if !ok {
		InternalServerError(w, "Database Access error")
	}

	usr, err := store.User().Authenticate(login, password)
	if err != nil {
		UnauthorizedError(w, "Invalid credentials")
		return
	}

	issueRefreshToken(ctx, usr, w)
}
func generateAccessToken(ctx context.Context, user *entities.User) (string, error) {
	config, _ := config.FromContext(ctx)
	claims := jwt.MapClaims{}
	claims["id"] = user.ID
	claims["email"] = user.Email
	claims["exp"] = time.Now().Add(time.Second * time.Duration(config.GetInt("JWT_EXPIRES"))).Unix()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return token.SignedString([]byte(config.GetString("JWT_SECRET")))
}

func issueRefreshToken(ctx context.Context, user *entities.User, w http.ResponseWriter) {
	config, _ := config.FromContext(ctx)
	tokenString, err := generateAccessToken(ctx, user)
	if err != nil {
		InternalServerError(w, fmt.Sprintf("error generating jwt token: %v", err))
		return
	}

	sendJSON(w, 200, &AuthResponse{
		Token:     tokenString,
		TokenType: "bearer",
		ExpiresIn: config.GetInt("JWT_EXPIRES"),
	})
}
