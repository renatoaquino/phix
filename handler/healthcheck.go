package handler

import (
	"net/http"
)

// Healthcheck endpoint for Zabbix or any monitor system
type Healthcheck struct{}

func (h Healthcheck) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("WORKING"))
}
