package handler

import (
	"net/http"
)

// Robots handler disallows access to crawler services
type Robots struct{}

func (ro Robots) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("User-agent: *\nDisallow: /\n"))
}
