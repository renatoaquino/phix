package handler

import (
	"net/http"

	"bitbucket.org/renatoaquino/phix.git/accessor"
	"bitbucket.org/renatoaquino/phix.git/errors"
)

// User the user handler
type User struct{}

func (u User) Me(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	usr, err := accessor.Me(ctx)
	if err != nil {
		errors.SendError(w, err)
		return
	}
	sendJSON(w, http.StatusOK, usr)
}
