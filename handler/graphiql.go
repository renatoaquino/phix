package handler

import "net/http"

// Content ...
var Content = []byte(`
<!DOCTYPE html>
<head>
  	<style>body {height: 100vh; margin: 0; width: 100%; overflow: hidden;}</style>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/sweetalert2/6.6.1/sweetalert2.css">
	  <link rel="stylesheet" href="https://cdn.jsdelivr.net/graphiql/0.10.1/graphiql.css">
	  <script src="https://cdn.jsdelivr.net/sweetalert2/6.6.1/sweetalert2.min.js"></script>
	  <script src="https://cdn.jsdelivr.net/fetch/2.0.1/fetch.min.js"></script>
	  <script src="https://cdn.jsdelivr.net/react/15.5.4/react.min.js"></script>
	  <script src="https://cdn.jsdelivr.net/react/15.5.4/react-dom.min.js"></script>
	  <script src="https://cdn.jsdelivr.net/graphiql/0.10.1/graphiql.min.js"></script>
  	<script>
   (function () {
      var STEPS = [
        {
            title: "Phix Host",
            text: "Please type the Phix host",
            input: "text",
            showCancelButton: false,
            inputPlaceholder: window.location.origin == 'file://' ? 'http://127.0.0.1:8888':window.location.origin,
        },
        {
            title: "Username",
            text: "Please type your Phix username",
            input: "text",
            showCancelButton: false,
        },
        {
            title: "Password",
            text: "Please type your Phix password",
            input: "password",
            showCancelButton: false,
        }
      ]

      function promptData(){
        swal.resetDefaults()
        swal.setDefaults({
            input: 'text',
            confirmButtonText: 'Next &rarr;',
            showCancelButton: false,
            animation: false,
            progressSteps: ['1', '2', '3']
        });
        swal.queue(STEPS).then(function (response) {
            swal.resetDefaults()
            var data = {
                endpoint: "",
                token: "",
                expires: 0
            };

            if(response[0] == ""){
                data.endpoint = window.location.origin == 'file://' ? 'http://127.0.0.1:8888':window.location.origin;
            }else{
                data.endpoint = response[0];
            }

            var dataForm = new FormData();
            dataForm.append('login', response[1]);
            dataForm.append('password', response[2]);

            return fetch(data.endpoint+'/login', {
                method: 'POST',
                body: dataForm
            }).then(function(response){
                if (response.status >= 200 && response.status < 300) {
                    return response
                } else if (response.status == 500 || response.status == 401) {
                    console.log(response);
                    throw new Error("Invalid information");
                }  
            }).then(function(response){
                return response.json();          
            }).then(function(response){
                console.log(response);
                data.token = response.access_token;
                data.expires = response.expires_in * 1000;
                function fetcher(params) {
                    var options = {
                    method: 'post',
                    headers: {'Accept': 'application/json', 'Content-Type': 'application/json','Authorization':'Bearer '+data.token},
                    body: JSON.stringify(params),
                    credentials: 'include',
                    };
                    return fetch(data.endpoint+'/graphql', options)
                    .then(function (res) { return res.json() });
                }
                var body = React.createElement(GraphiQL, {fetcher: fetcher, query: '', variables: ''});
                ReactDOM.render(body, document.body);
            }).catch(function(ex){
                console.log('request failed', ex);
                swal('Error',ex,'error');
                return promptData();
            });
        });
      };

        document.addEventListener('DOMContentLoaded', function () {
            promptData();
        });
    }());
  	</script>
</head>
<body>
</body>
`)

// ServeGraphiQL is a handler function for HTTP servers
func ServeGraphiQL(res http.ResponseWriter, req *http.Request) {
	res.Write(Content)
}
