package errors

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// Error is an error with a message
type Error struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func (e Error) Error() string {
	return fmt.Sprintf("%v: %v", e.Code, e.Message)
}

var (
	// BadRequest error message
	BadRequest = &Error{Message: "Bad Request", Code: 400}
	// Database error message
	Database = &Error{Message: "Database Access error", Code: 500}
	// TokenRequired error message
	TokenRequired = &Error{Message: "This endpoint requires a Bearer token", Code: 401}
	// InvalidToken error message
	InvalidToken = &Error{Message: "Invalid token", Code: 401}
	// Unauthrorized error message
	Unauthrorized = &Error{Message: "Invalid token", Code: 401}
	// UnprocessableEntity error message
	UnprocessableEntity = &Error{Message: "Unprocessable Entity", Code: 422}
	// NotFound 404 error message
	NotFound = &Error{Message: "Not Found", Code: 404}
)

// SendError helper function to write an http error.
func SendError(w http.ResponseWriter, err *Error) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(err.Code)
	encoder := json.NewEncoder(w)
	encoder.Encode(err.Message)
}
