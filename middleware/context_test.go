package middleware

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"
)

type key int

var valkey key = 0
var message string = "Hello"

type fakeHandler struct {
	request http.Request
}

func (f *fakeHandler) SaveRequest(w http.ResponseWriter, r *http.Request) {
	f.request = *r
}

func TestContextAssignment(t *testing.T) {
	fh := fakeHandler{}
	req, err := http.NewRequest("GET", "/test", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Dummy context just to hold a verifiable value
	ctx := context.Background()
	ctx = context.WithValue(ctx, valkey, message)

	rr := httptest.NewRecorder()
	handler := http.Handler(ContextHandler(ctx, http.HandlerFunc(fh.SaveRequest)))

	handler.ServeHTTP(rr, req)

	// Does the request has our ccontext?
	gotctx := fh.request.Context()
	if gotctx != ctx {
		t.Errorf("Wrong context found: got %p want %p",
			ctx, gotctx)
	}

	// Does the context has our dummy value?
	if gotctx.Value(valkey).(string) != message {
		t.Errorf("Wrong context value found: got %v want %v",
			gotctx.Value(valkey).(string), message)
	}
}
