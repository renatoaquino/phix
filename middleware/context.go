package middleware

import (
	"context"
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
)

type keytype int

const jwtkey keytype = 0

// ContextHandler attaches an context to the request
func ContextHandler(ctx context.Context, h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r = r.WithContext(ctx)
		h.ServeHTTP(w, r)
	})
}

func GetToken(ctx context.Context) *jwt.Token {
	return ctx.Value(jwtkey).(*jwt.Token)
}

func setToken(ctx context.Context, tkn *jwt.Token) context.Context {
	return context.WithValue(ctx, jwtkey, tkn)
}

func TokenClaims(tkn *jwt.Token) jwt.MapClaims {
	return tkn.Claims.(jwt.MapClaims)
}
