package middleware

import (
	"fmt"
	"net/http"
	"regexp"

	"bitbucket.org/renatoaquino/phix.git/config"
	"bitbucket.org/renatoaquino/phix.git/errors"
	jwt "github.com/dgrijalva/jwt-go"
)

var bearerRegexp = regexp.MustCompile(`^(?:B|b)earer (\S+$)`)

// RequireAuthentication middleware to enforce user identification
// Wrap the a handler with this HandlerFunc to enforce a valid user
// identification.
// It populates r.Context() with a token that could be retrived with
// the helper handler.getToken(context.Context)
func RequireAuthentication(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		errors.SendError(w, errors.TokenRequired)
		return
	}

	matches := bearerRegexp.FindStringSubmatch(authHeader)
	if len(matches) != 2 {
		errors.SendError(w, errors.TokenRequired)
		return
	}

	config, _ := config.FromContext(ctx)
	token, err := jwt.Parse(matches[1], func(token *jwt.Token) (interface{}, error) {
		if token.Header["alg"] != "HS256" {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(config.GetString("JWT_SECRET")), nil
	})
	if err != nil {
		errors.SendError(w, errors.InvalidToken)
		return
	}
	ctx = setToken(ctx, token)
	*r = *r.WithContext(ctx)
}

func RequireAuthFunc(fn func(w http.ResponseWriter, r *http.Request)) http.Handler {
	return RequireAuth(http.HandlerFunc(fn))
}

func RequireAuth(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//fmt.Printf("%p\n", r)
		RequireAuthentication(w, r)
		//fmt.Printf("%+v\n", r.Context())
		//fmt.Printf("%p\n", r)
		h.ServeHTTP(w, r)
	})
}
